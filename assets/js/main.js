//
const inquiryUrl = '/inquiry/postmail.cgi';

if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = Array.prototype.forEach;
}

document.addEventListener("DOMContentLoaded", function () {
  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll(".navbar-burger"),
    0
  );
  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener("click", function () {
        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);
        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle("is-active");
        $target.classList.toggle("is-active");
      });
    });
  }

  // for modal
  $("#close-modal-btn").on("click", function(event) {
    $('#formModal').removeClass('is-active');
    $('html').removeClass('is-clipped');
  })
});

// Smooth Anchor Scrolling
$(document).on("click", 'a[href^="#"]', function (event) {
  event.preventDefault();
  $("html, body").animate({
      scrollTop: $($.attr(this, "href")).offset().top
    },
    500
  );
});

// When the user scrolls down 20px from the top of the document, show the scroll up button
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("toTop").style.display = "block";
  } else {
    document.getElementById("toTop").style.display = "none";
  }
}

// Preloader
$(document).ready(function ($) {
  $(".preloader-wrapper").fadeOut();
  $("body").removeClass("preloader-site");
});
$(window).on("load", function () {
  var Body = $("body");
  Body.addClass("preloader-site");
});


// tab in profile
document.querySelectorAll("#nav-profile li").forEach(function (navEl) {
  navEl.onclick = function () {
    toggleTabProfile(this.id, this.dataset.target);
  }
});


function toggleTabProfile(selectedNav, targetId) {
  var navEls = document.querySelectorAll("#nav-profile li");


  navEls.forEach(function (navEl) {
    if (navEl.id == selectedNav) {
      navEl.classList.add("is-active");
    } else {
      if (navEl.classList.contains("is-active")) {
        navEl.classList.remove("is-active");
      }
    }
  });

  var tabs = document.querySelectorAll("#nav-profile ~ .tab-content .tab-pane");

  tabs.forEach(function (tab) {
    if (tab.id == targetId) {
      tab.style.display = "block";
    } else {
      tab.style.display = "none";
    }
  });

  AOS.refresh();
}


// tab in achievement
document.querySelectorAll("#nav-achievement li").forEach(function (navEl) {
  navEl.onclick = function () {
    toggleTabAchievement(this.id, this.dataset.target);
  }
});


function toggleTabAchievement(selectedNav, targetId) {
  var navEls = document.querySelectorAll("#nav-achievement li");


  navEls.forEach(function (navEl) {
    if (navEl.id == selectedNav) {
      navEl.classList.add("is-active");
    } else {
      if (navEl.classList.contains("is-active")) {
        navEl.classList.remove("is-active");
      }
    }
  });


  var tabs = document.querySelectorAll("#nav-achievement ~ .tab-content .tab-pane");


  tabs.forEach(function (tab) {
    if (tab.id == targetId) {
      tab.style.display = "block";
    } else {
      tab.style.display = "none";
    }
  });

  AOS.refresh();
}

//
// formModal = document.getElementById('formModal');

function showModal(message) {
  // const modalBody = formModal.getElementsByClassName('modal-card-body')[0];

  // modalBody.innerHTML = '<p>' + message + '</p>';
  const $modalForm = $('#formModal');
  $modalForm.find('.dialog-body').html('<i class="fas fa-exclamation-circle fa-2x header-icon"></i><p>' + message + '</p>');
  $modalForm.addClass('is-active');
  $('html').addClass('is-clipped');
  // formModal.classList.add('is-active');
}

// お問い合わせフォーム
function validateContents() {
  //
  const formEl = document.getElementById('inquiryForm');
  const nameEl = document.getElementsByName('name')[0];
  const emailEl = document.getElementsByName('email')[0];
  const messageEl = document.getElementsByName('メッセージ')[0];
  // 入力エラー文字列
  let invalidFieldNames = [];

  if (!nameEl.value) {
    invalidFieldNames.push('お名前');
  }
  if (!emailEl.value) {
    invalidFieldNames.push('メールアドレス');
  }
  if (!messageEl.value) {
    invalidFieldNames.push('お問い合わせ内容');
  }
  if (!!invalidFieldNames.length) {
    showModal(invalidFieldNames.join('、') + 'を入力してください。');
    return false;
  }
  // POSTコマンド設定（スパム対策用）
  formEl.action = inquiryUrl;

  return true;
}
