'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');

sass.compiler = require('node-sass');

gulp.task('top', () => {
  return gulp.src(['index.html', 'favicon.ico'])
    .pipe(gulp.dest('./docroot'))
});

gulp.task('assets', () => {
  return gulp.src('./assets/**/*')
    .pipe(gulp.dest('./docroot/assets'))
});

gulp.task('sass', () => {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(postcss([autoprefixer()]))
    .pipe(gulp.dest('./docroot/assets/css'));
});

gulp.task('sass:watch', () => {
  gulp.watch('./scss/**/*.scss', gulp.parallel('sass', 'log'));
});

gulp.task('log', () => {
  return console.log('DONE')
});

gulp.task('default', gulp.parallel('top', 'assets', 'sass'));